<?php

namespace App\Command;

use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class CrawlJobsCommand extends Command
{
    const PROFESSIONAL = 'professional';
    const GRADUATE = 'graduate';

    protected static $defaultName = 'app:crawl-jobs';

    protected $city = 'stockholm';
    protected $senorLevelYears = 7;
    protected $middleLevelYears = 3;

    protected function configure()
    {
        $this->setDescription('Crawl jobs from spotify jobs website.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $jobsList = [];

        try {
            // get jobs list as json
            $client = new Client(['base_uri' => 'https://api-dot-new-spotifyjobs-com.nw.r.appspot.com']);
            $response = $client->request('GET', '/wp-json/animal/v1/job/search?q=&l=' . $this->city . '&c=&j=');
            $decodedJobs = json_decode($response->getBody());

            $client = new Client(['base_uri' => 'https://www.spotifyjobs.com/']);

            // iterate throught each job
            foreach ($decodedJobs->result as $job) {
                $years = 'n/a';
                $content = '';

                // get info from single job's page
                $response = (string)$client->request('GET', 'jobs/' . $job->id)->getBody();
                $crawler = new Crawler($response);
                $jsonInfo = json_decode($crawler->filter('body > script#__NEXT_DATA__')->html());
                $jobDescription = $jsonInfo->props->pageProps->job->content->description;
                $lists = $jsonInfo->props->pageProps->job->content->lists;

                foreach ($lists as $list) {
                    if (strpos(strtolower($list->text), 'who you are') !== false) {
                        $content = $list->content;
                        preg_match('/You have [0-9]+/i', $content, $r);
                        if (isset($r[0])) {
                            $years = (int)preg_replace("/[^0-9]/", "", $r[0]);
                            break;
                        }
                        preg_match('/At least [0-9]+/i', $content, $r);
                        if (isset($r[0])) {
                            $years = (int)preg_replace("/[^0-9]/", "", $r[0]);
                            break;
                        }
                        preg_match('/[0-9]+\s?\+/i', $content, $r);
                        if (isset($r[0])) {
                            $years = (int)preg_replace("/[^0-9]/", "", $r[0]);
                            break;
                        }
                        break;
                    }
                }

                // if are required any year of experience means need a professionals
                if (is_int($years) && $years > 0) {
                    $jobType = self::PROFESSIONAL;
                } else {
                    if (strpos(strtolower($job->text), 'senior') !== false) {
                        $years = $this->senorLevelYears;
                        $jobType = self::PROFESSIONAL;
                    } else {

                        $jobType = self::GRADUATE;
                        preg_match('/(have experience)|(you are experienced)|(years of working experience)|(an experienced web)/i', $content, $r);
                        if (isset($r[0])) {
                            $years = $this->middleLevelYears;
                            $jobType = self::PROFESSIONAL;
                        }
                        preg_match('/(significant professional experience)|(have solid experience)|(have extensive experience)|(have validated experience)/i', $content, $r);
                        if (isset($r[0])) {
                            $years = $this->senorLevelYears;
                            $jobType = self::PROFESSIONAL;
                        }
                    }
                }

                array_push($jobsList, [
                    'url' => 'https://www.spotifyjobs.com/jobs/' . $job->id,
                    'headline' => $job->text,
                    'description' => $jobDescription,
                    'type' => $jobType,
                    'years' => $years
                ]);

            }

            echo json_encode($jobsList);
            return Command::SUCCESS;

        } catch (\Exception $exception) {
            echo 'Exception: ' . $exception->getMessage();
            return Command::FAILURE;
        }
    }
}
